gemdropx (0.9-10) unstable; urgency=medium

  * Simplify rules using dh and install files.
  * Include manpage as file, drop manpage patches.

 -- Christian T. Steigies <cts@debian.org>  Sun, 12 Jan 2025 19:42:07 +0100

gemdropx (0.9-9.1) unstable; urgency=medium

  * Non-maintainer upload with permission from maintainer.
  * Remove requirement for root when building the package.
    (Closes: #1089335)

 -- Niels Thykier <niels@thykier.net>  Tue, 31 Dec 2024 14:09:35 +0000

gemdropx (0.9-9) unstable; urgency=medium

  [ Debian Janitor ]
  * Use versioned copyright format URI.
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ Christian T. Steigies ]
  * Bump debhelper to 13
  * update Standards-Version to 4.6.2 (no changes)

 -- Christian T. Steigies <cts@debian.org>  Fri, 06 Jan 2023 13:30:08 +0100

gemdropx (0.9-8) unstable; urgency=medium

  [ Helmut Grohne ]
  * Fix FTCBFS: Let dh_auto_build pass cross tools to make.
    (Closes: #913927)

  [ Christian T. Steigies ]
  * updated Standards-Version to 4.2.1, drop menu file
  * add patch description
  * add Keywords to Desktop file
  * use DEB_CFLAGS and CPPFLAGS and enable all hardening options
  * update watch file

 -- Christian T. Steigies <cts@debian.org>  Sun, 25 Nov 2018 22:49:47 +0100

gemdropx (0.9-7) unstable; urgency=low

  * updated Standards-Version to 3.9.6
  * enable hardening with debhelper 9

 -- Christian T. Steigies <cts@debian.org>  Thu, 23 Oct 2014 22:55:09 +0200

gemdropx (0.9-6) unstable; urgency=low

  * repackage from scratch with dh
  * switch to debhelper 8
  * switch to dpkg-source 3.0 (quilt) format
  * updated Standards-Version to 3.9.2 (no changes)
  * copyright: refer to GPL-2 and include detailed information from AUTHORS
  * add ${misc:Depends} to dependencies
  * upgrade watch file to version 3 format

 -- Christian T. Steigies <cts@debian.org>  Mon, 05 Sep 2011 23:04:37 +0200

gemdropx (0.9-5) unstable; urgency=low

  * change menu section to Games/Block,
    if my crystal ball is correct this (closes: #307133)
  * use debian/compat and use compat version 5
  * add a desktop file
  * do not ignore make clean error
  * updated Standards-Version to 3.7.3

 -- Christian T. Steigies <cts@debian.org>  Fri, 14 Mar 2008 00:08:28 +0100

gemdropx (0.9-4) unstable; urgency=low

  * updated watch file
  * updated Standards-Version to 3.7.0 (no changes)
  * use current debhelper

 -- Christian T. Steigies <cts@debian.org>  Thu,  4 May 2006 06:47:37 +0000

gemdropx (0.9-3) unstable; urgency=low

  * patch from Bill to honor "SDL_QUIT" events (closes: #299431)
  * copyright is with the author

 -- Christian T. Steigies <cts@debian.org>  Sat, 19 Mar 2005 00:08:30 +0100

gemdropx (0.9-2) unstable; urgency=low

  * use TARGET and CURDIR in rules
  * data files go in /usr/share/games/gemdropx
  * dh_compat=4
  * dh_installman instead of dh_installmanpages
  * updated watch file (use ftp link instead of http)
  * install menu icon to /usr/share/pixmaps
  * updated Standards-Version to 3.6.1.0 (no changes)

 -- Christian T. Steigies <cts@debian.org>  Sun, 13 Mar 2005 15:42:20 +0100

gemdropx (0.9-1) unstable; urgency=low

  * recreated score and nosound patch from Zack
  * apply patch from Steve Kemp to fix buffer overflow (closes: #203244)

 -- Christian T. Steigies <cts@debian.org>  Mon, 28 Jul 2003 23:48:23 -0400

gemdropx (0.9-0) unstable; urgency=low

  * new upstream version
  * patch install rule in Makefile
  * need to re-add nosound patch

 -- Christian T. Steigies <cts@debian.org>  Mon, 17 Jun 2002 00:16:06 -0400

gemdropx (0.7-8) unstable; urgency=low

  * applied patch from Zack Weinberg <zack@codesourcery.com> which fixes:
    score problem, adds --nosound, better sound and joystick handling
    see source package for patch, thanks Zack! (closes: #115203)

 -- Christian T. Steigies <cts@debian.org>  Sun, 14 Oct 2001 00:48:12 -0400

gemdropx (0.7-7) unstable; urgency=low

  * Build-Depends on libsdl1.2-dev (>= 1.2.2-3.1),
    libsdl-mixer1.2-dev (>= 1.2.0-1.1) due to SDL overhaul

 -- Christian T. Steigies <cts@debian.org>  Wed, 10 Oct 2001 23:42:25 -0400

gemdropx (0.7-6) unstable; urgency=low

  * remove (unneeded) dh_suidregister from debian/rules
  * build with SDL1.2
  * update standards version
  * do not ship INSTALL.txt

 -- Christian T. Steigies <cts@debian.org>  Mon, 16 Jul 2001 18:36:48 -0400

gemdropx (0.7-5) unstable; urgency=low

  * correct the description (closes: #76075)

 -- Christian T. Steigies <cts@debian.org>  Sat,  4 Nov 2000 01:46:28 +0100

gemdropx (0.7-4) unstable; urgency=low

  * use libSDL-mixer for sound
  * place icon for the menu system in /usr/X11R6/include/X11/pixmaps/

 -- Christian T. Steigies <cts@debian.org>  Tue, 18 Jan 2000 23:30:21 +0100

gemdropx (0.7-3) unstable; urgency=low

  * README changed its name to README.txt, change the manpage accordingly
  * add a note to README.Debian that there might be a problem when using
    sawmill as the window manger (I could not reproduce this) closes: #54637

 -- Christian T. Steigies <cts@debian.org>  Fri, 14 Jan 2000 10:45:12 +0100

gemdropx (0.7-2) unstable; urgency=low

  * use libsdl1.0, compiles now also on m68k

 -- Christian T. Steigies <cts@debian.org>  Tue, 11 Jan 2000 18:43:45 +0100

gemdropx (0.7-1) unstable; urgency=low

  * Initial Release.

 -- Christian T. Steigies <cts@debian.org>  Wed, 29 Dec 1999 01:03:27 +0100
